var socket;
var insert;
var maintext;
var timerId = undefined ; 
var butStop;
var cache;
var timerResize;
var scrollId;

function Copy() {
	new Clipboard('#copy-button');
};

function setCopyLink() {
	var begin = "http://" + window.location.host + "/?filename="
			+ $('#filename').val() + "&filter=" + $('#filter').val();
	$('#post-shortlink').val(begin);
};

function addText(text) {
	insert.before(text);
	scrollId.scrollIntoView({});
}

function Start() {
	cache = "";
	console.log("TimerId=" + timerId);
	socket.send('start');
	if (timerId == undefined ) {
		timerId = setInterval(ontimer, 200);
		console.log("set timer" + timerId);
	}
}

function ontimer() {
	
	scrollId.scrollIntoView({});
}

function Stop() {
	clearInterval(timerId);
	console.log("stop timer" + timerId);
	timerId = undefined; 
	socket.send('stop');
	setTimeout(theEnd, 500);

	if (maintext.html().length > 1000000) {
		var len = maintext.html().length;
		var temp = maintext.html().substring(1000000);
		maintext.html(temp);
	}
}

function theEnd() {
	insert = $("#footer2");
	maintext = $("#mainest");
	scrollId = document.getElementById('footer2');
	scrollId.scrollIntoView({});
}

function Reconnect() {

	window.location = "http://" + window.location.host + "/?filename="
			+ $('#filename').val() + "&filter=" + $('#filter').val();
}

function grep() {
	if ($('#grep').is(":checked")) {
		addText('<div class="warn" >Поиск будет от начала файла.</div>');
		socket.send('grepon');
		return;
	}
	addText('<div class="warn" >Поиск будет с конца файла.</div>');
	socket.send('grepoff');
}

function ignoreCase() {
	if ($('#ignoreCase').is(":checked")) {
		addText('<div class="warn" >Регистр учитывается</div>');
		socket.send('iCaseOn');
		return;
	}
	addText('<div class="warn" >Игнорируем регистр</div>');
	socket.send('iCaseOff');
}

function Clear() {
	console.log("clear page");
	$('#mainest').empty().append('<div id="footer2"></div>');
	insert = $("#footer2");
	maintext = $("#mainest");
	scrollId = document.getElementById('footer2');
	cache = "";
};

$(document).ready(
		function() {
			(window.onpopstate = function() {
				var match, pl = /\+/g, // Regex for replacing addition symbol with a space
				search = /([^&=]+)=?([^&]*)/g, decode = function(s) {
					return decodeURIComponent(s.replace(pl, " "));
				}, query = window.location.search.substring(1);

				urlParams = {};
				while (match = search.exec(query))
					urlParams[decode(match[1])] = decode(match[2]);
			})();
			$('#filter').val(urlParams["filter"]);
			$('#filename').val(urlParams["filename"]);
			$('#selectfile').val(urlParams["filename"])

			$("#selectfile").change(function() {
				$('#filename').val($('#selectfile').val())
				socket.send('file:' + $('#filename').val());
				setCopyLink();
			});

			$("#filename").change(function() {
				socket.send('file:' + $('#filename').val());
				setCopyLink();
			});

			$("#filter").change(function() {
				socket.send('filter:' + $('#filter').val());
				setCopyLink();
			});

			setCopyLink();
			insert = $("#footer2");
			maintext = $("#mainest");
			butStop = $("#buttonStop");
			reconnect = $("#reconnect");
			scrollId = document.getElementById('footer2');
			$('#grep').attr("checked", false);

			socket = new WebSocket("ws://" + window.location.host
					+ "/logservice");

			socket.onopen = function() {
				addText("<div class='welcome' >Соединение установлено.</div>");
				socket.send('file:' + $('#filename').val());
				socket.send('filter:' + $('#filter').val());
			};

			socket.onclose = function(event) {
				if (event.wasClean) {
					addText('<div class="welcome" >Соединение закрыто.</div>');
				} else {
					addText('<div class="error">Обрыв соединения</div>'); // например, "убит" процесс сервера
				}
				addText('<div class="warn">Код: ' + event.code + ' причина: '
						+ event.reason + '</div>');
			};

			socket.onmessage = function(e) {

				insert.before(e.data);

			};

			socket.onerror = function(error) {
				addText("Ошибка " + error.message);
			};

		});
