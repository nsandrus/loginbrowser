package ru.nsandrus.entity;

import ru.nsandrus.file.ObserverFile;
import ru.nsandrus.filter.FilterMessage;
import ru.nsandrus.manager.BuilderMessage;

public class Terminal {
	
	private String filename;
	private ObserverFile publisher;
	private boolean isFromBegin;
	private FilterMessage filterMessage = new FilterMessage();
	private BuilderMessage builderMes = new BuilderMessage();
	private boolean isIgnoreCase;
	
	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
		builderMes.SetFilename(filename);
	}

	public ObserverFile getObserver() {
		return publisher;
	}

	public void setObserver(ObserverFile publisher) {
		this.publisher = publisher;
		publisher.initialize(this);
	}
	

	public boolean isFromBegin() {
		return isFromBegin;
	}

	public void setFromBegin(boolean isFromBegin) {
		this.isFromBegin = isFromBegin;
	}

	public void setFilter(String filter) {
		filterMessage.setNewFilter(filter);
	}

	public FilterMessage getFilterMessage() {
		return filterMessage;
	}

	public String buildMessage(String line){
		return builderMes.getMessage(line).toString();
	}

	public boolean isIgnoreCase() {
		return isIgnoreCase;
	}

	public void setIgnoreCase(boolean isIgnoreCase) {
		this.isIgnoreCase = isIgnoreCase;
	}

	
	
}
