package ru.nsandrus.file;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.LinkedList;

import io.reactivex.functions.Consumer;
import io.reactivex.subjects.PublishSubject;
import ru.nsandrus.entity.Terminal;

public class ObserverFile implements Runnable {

	private LinkedList<String> bufferMessages = new LinkedList<>();
	private volatile boolean isAlive = false;
	private String filename;
	private String charset;
	private volatile boolean isFromBegin;
	private PublishSubject<String> publishSubject;
	private Consumer<String> consumer;
	private Consumer<Throwable> consumerError;

	public ObserverFile(Consumer<String> consumer, Consumer<Throwable> error) {
		this.consumer = consumer;
		this.consumerError = error;
		publishSubject = PublishSubject.create();
	}

	@Override
	public void run() {
		if (isAlive) {
			return;
		}
		isAlive = true;
		publishSubject.subscribe(consumer, consumerError);
		
		RandomAccessFile fileCursor = null;
		try {
			fileCursor = new RandomAccessFile(filename, "r");
			if (!isFromBegin) {
				fileCursor.seek(fileCursor.length());
			}
		} catch (IOException e) {
			publishSubject.onError(e);
			isAlive = false;;
		}
		
		mainLoop(fileCursor);

		publishSubject.onError(new Exception("Поток наблюдения остановлен"));
	}

	private void mainLoop(RandomAccessFile fileCursor) {
		while (isAlive) {
			try {
				readMessageByByte(fileCursor);
			} catch (IOException e) {
				e.printStackTrace();
			}
			String mes = bufferMessages.poll();
			if (mes != null) {
				publishSubject.onNext(mes);
			} else {
				sleep(500);
			}
		}
	}

	private void sleep(int ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
		}
		
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public void initialize(Terminal terminal) {
		filename = terminal.getFilename();
		isFromBegin = terminal.isFromBegin();

		if (filename.toUpperCase().contains("SERVER")) {
			charset = "CP1251";
		} else {
			charset = "UTF8";
		}

		if (filename.toUpperCase().contains("NOTIFICA")) {
			charset = "CP1251";
		}

	}

	private void readMessageByByte(RandomAccessFile fileCursor) throws IOException {
		long sizeBuf = 0;
		long newSizeeBuf = fileCursor.length() - fileCursor.getFilePointer();
		if (newSizeeBuf > 0) {
			sizeBuf = newSizeeBuf;
		}

		if (sizeBuf == 0) {
			return;
		}

		byte[] byteBuffer = new byte[(int) sizeBuf];
		fileCursor.read(byteBuffer);
		String res = new String(byteBuffer, charset);
		for (String line : res.split("\n")) {
			bufferMessages.add(line);

		}
		return;
	}

	public boolean isAlive() {
		return isAlive;
	}

	public void stopWork() {
		isAlive = false;
	}

}
