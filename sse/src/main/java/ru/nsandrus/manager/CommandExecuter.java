package ru.nsandrus.manager;

import org.springframework.web.socket.WebSocketSession;

import ru.nsandrus.color.MessageColorHTML;
import ru.nsandrus.entity.Terminal;
import ru.nsandrus.file.ObserverFile;
import ru.nsandrus.sender.SenderMessage;

public class CommandExecuter {

	private static final String STOP = "stop";
	private static final String GREPOFF = "grepoff";
	private static final String GREPON = "grepon";
	private static final String ICASEON = "iCaseOn";
	private static final String ICASEOFF = "iCaseOff";
	private static final String FILTER = "filter:";
	private static final String FILE = "file:";
	private static final String START = "start";

	public static void executeCommand(String message, Terminal terminal, WebSocketSession session) {
		if (message.startsWith(START)) {
			startFileObserver(session, terminal);
		} else if (message.startsWith(FILE)) {
			stopObserver(terminal);
			terminal.setFilename(message.substring(5));
		} else if (message.startsWith(FILTER)) {
			terminal.setFilter(message.substring(7));
		} else if (message.startsWith(GREPON)) {
			terminal.setFromBegin(true);
		} else if (message.startsWith(GREPOFF)) {
			terminal.setFromBegin(false);
		} else if (message.startsWith(ICASEON)) {
			terminal.setIgnoreCase(true);
		} else if (message.startsWith(ICASEOFF)) {
			terminal.setIgnoreCase(false);
		} else if (message.startsWith(STOP)) {
			stopObserver(terminal);
		}

	}

	public static void startFileObserver(WebSocketSession session, final Terminal terminal) {
		if (!isPassedSetting(session, terminal)) {
			return;
		}

		ObserverFile observer = getNewObserver(session, terminal);

		terminal.setObserver(observer);
		new Thread(observer).start();
		SenderMessage.sendInfo(session, "Запущен поток наблюдения!");

	}

	private static boolean isPassedSetting(WebSocketSession session, final Terminal terminal) {
		if (terminal.getObserver() != null && terminal.getObserver().isAlive()) {
			return false;
		}
		if (terminal.getFilename().isEmpty()) {
			SenderMessage.sendError(session, "Имя файла не указано");
			return false;
		}
		return true;
	}

	private static ObserverFile getNewObserver(WebSocketSession session, final Terminal terminal) {
		ObserverFile observer = new ObserverFile(line -> {
			chainLogic(session, terminal, line);
		}, exception -> {
			SenderMessage.sendError(session, exception.getMessage());
		});
		return observer;
	}

	private static void chainLogic(WebSocketSession session, final Terminal terminal, String line) {
		String mes = terminal.buildMessage(line);

		if (!mes.isEmpty() && isPassFilter(terminal, mes)) {
			SenderMessage.sendMessage(session, MessageColorHTML.getMessageColorized(mes));
		}
	}

	private static boolean isPassFilter(final Terminal terminal, String mes) {
		if (terminal.isIgnoreCase()) {
			return terminal.getFilterMessage().isPassIgnoreCase(mes);
		}
		return terminal.getFilterMessage().isPass(mes);
	}

	private static void stopObserver(Terminal terminal) {
		if (terminal.getObserver() != null) {
			terminal.getObserver().stopWork();
		}
	}

}
