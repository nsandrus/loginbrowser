package ru.nsandrus.manager;


public class BuilderMessage {

	private String charBeginLine;
	private String charEndLine;
	private boolean isGf;
	StringBuilder message = new StringBuilder();
	private boolean isBeginBuild;
	
	public void SetFilename(String filename) {
		
		if (filename.toUpperCase().contains("SERVER")) {
			charBeginLine = "[#|";
			charEndLine = "|#]";
			isGf = true;
		} else {
			 charBeginLine = "";
			 charEndLine = "\n";
			 isGf = false;
		}
	}
	
	public String getMessage(String line)  {

			if (line == null) {
				return "";
			}

			if (!isGf) {
				return line;
			}
			
			if (isFoundStartMessage(line)) {
				message.setLength(0);
				isBeginBuild = true;
			}
			
			if (isBeginBuild) {
				message.append(line);
			}
						
			
			if (lineIsFull()) {
				message.append("\n");
				String result = message.toString();
				message.setLength(0);
				isBeginBuild = false;
				return result;
			} 

			if (isFoundEndMessage()) {
				message.setLength(0);
			}

		return "";
	}

	
	private boolean isFoundEndMessage() {
		return (message.indexOf(charEndLine) >= 0) ? true : false;
	}

	private boolean isFoundStartMessage(String line) {
		return (line.indexOf(charBeginLine) >= 0) ? true : false;
	}

	private boolean lineIsFull() {
		return (message.indexOf(charBeginLine) == 0 && message.indexOf(charEndLine) > 0) ? true : false;
	}
}
