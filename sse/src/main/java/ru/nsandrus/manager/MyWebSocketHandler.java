package ru.nsandrus.manager;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import ru.nsandrus.entity.Terminal;

@Component
public class MyWebSocketHandler extends TextWebSocketHandler {

	Logger LOG = Logger.getLogger(MyWebSocketHandler.class);
	private volatile Map<WebSocketSession, Terminal> map = new ConcurrentHashMap<>();

	@Override
	public void handleTransportError(WebSocketSession session, Throwable throwable) throws Exception {
		LOG.error("error occured at sender " + session, throwable);
		map.remove(session);

	}

	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
		LOG.info(String.format("Session %s closed because of %s", session.getId(), status.getReason()));
		Terminal terminal = map.get(session);
		map.remove(session);

		CommandExecuter.executeCommand("stop", terminal, session);

	}

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		LOG.info("Connected ... " + session.getId());
		
		map.put(session, new Terminal());

	}

	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage textMessage) throws Exception {
		LOG.info("message received: " + textMessage.getPayload());
		String message = textMessage.getPayload();

		Terminal terminal = map.get(session);
		
		if (terminal == null) {
			LOG.warn("not found terminal " + session.getId());
			return;
		}
		CommandExecuter.executeCommand(message, terminal, session);
	}

}
