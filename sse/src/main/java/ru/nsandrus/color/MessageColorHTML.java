package ru.nsandrus.color;

import org.apache.commons.lang3.StringUtils;

public class MessageColorHTML {

	public static String getMessageColorized(String message) {

		String escapedMessage = getEscapedMessage(message);

		String[] chunks = escapedMessage.split("\\|");
		int countColumn = chunks.length - 1;
		StringBuilder result = new StringBuilder();
		int column = 0;
		String tag = "div";
		if (chunks[0].equals("[#") && countColumn > 1) {
			tag = "p";
		}
		StringBuilder colorChunk = new StringBuilder();
		result.append("<").append(tag).append(" class=\"gf\">");
		for (String chunk : chunks) {

			if (column == 2) {
				result.append("<span class=\"").append(chunk).append("\">");
			} else if (countColumn > 8 && column >= 6) {
				result.append("<span class=\"column6\">");
			} else {
				result.append("<span class=\"column").append(column).append("\">");
			}

			if (column == 5 || column == 6) {
				colorChunk.setLength(0);
				colorChunk = getSpecColor(colorChunk.append(chunk), "&gt;", "&lt;", "spec");
				colorChunk = getSpecColor(colorChunk, "[", "]", "quad");
				colorChunk = getSpecColor(colorChunk, "Service Assembly Name=", ";", "assemblyname");
				colorChunk = getSpecColor(colorChunk, "BPEL Process Name=", ";", "bpelname");
				colorChunk = getSpecColor(colorChunk, "Activity Name=", ";", "activyname");

				result.append(colorChunk);
			} else {
				result.append(chunk);
			}
			result.append("</span>");

			if (countColumn != column) {
				result.append("|");
			}
			column++;
		}
		result.append("</").append(tag).append(">");

		return result.toString();
	}

	private static String getEscapedMessage(String message) {
		String escaped = org.apache.commons.lang3.StringEscapeUtils.escapeHtml4(message);

		String line = StringUtils.replace(escaped, "\r", "");
		line = StringUtils.replace(line, "\n", "<br/>");
		line = StringUtils.replace(line, "\t", "&emsp;");
		line = StringUtils.replace(line, "  ", "&nbsp;&nbsp;");
		return line;
	}

	private static StringBuilder getSpecColor(StringBuilder chunk, String openSimbol, String closeSimbol, String classText) {

		StringBuilder strb = new StringBuilder(chunk);
		int leftPos = 0, rightPos = 0, pos = 0;
		int oneSpace = 1;

		pos = strb.indexOf(openSimbol);
		if (pos < 0) {
			return chunk;
		}

		String tagBegin = "<span class=\"" + classText + "\">";
		String tagEnd = "</span>";

		while (pos < strb.length()) {
			leftPos = strb.indexOf(openSimbol, pos);
			rightPos = strb.indexOf(closeSimbol, pos);
			if (leftPos < 0 || rightPos < 0) {
				break;
			}

			if (rightPos - leftPos > oneSpace) {
				strb = strb.insert(leftPos + openSimbol.length(), tagBegin);
				strb = strb.insert(rightPos + tagBegin.length(), tagEnd);
				pos += tagBegin.length() + tagEnd.length();
			} else {
				pos = rightPos + closeSimbol.length();
			}

		}

		return strb;
	}

}