package ru.nsandrus.sender;

import java.io.IOException;

import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;

public class SenderMessage {


	public static void sendMessage(WebSocketSession session, String colorMes) {
		WebSocketMessage<String> message = new TextMessage(colorMes);
		try {
			session.sendMessage(message);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public static void sendError(WebSocketSession session, String colorMes) {
		sendMessage(session,"<div class='errorEmitter'>" + colorMes + "</div" );
	}
	
	public static void sendInfo(WebSocketSession session, String colorMes) {
		sendMessage(session, "<div class='welcome'>" + colorMes + "</div" );
	}

}
