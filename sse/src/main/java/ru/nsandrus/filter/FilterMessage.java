package ru.nsandrus.filter;

import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;
import static ru.nsandrus.filter.FilterMessage.TypeWord.AND;
import static ru.nsandrus.filter.FilterMessage.TypeWord.NOT;
import static ru.nsandrus.filter.FilterMessage.TypeWord.OR;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class FilterMessage {

	enum TypeWord {
		OR, AND, NOT
	};

	List<String> listOr = new ArrayList<>();
	List<String> listAnd = new ArrayList<>();
	List<String> listNot = new ArrayList<>();

	public void setNewFilter(String parameter) {
		listOr.clear();
		listAnd.clear();
		listNot.clear();
	
		
		String[] arrayWords = parameter.split(" ");
		boolean isSpanText = false;
		String blockText = "";
		TypeWord type = null;

		for (String word : arrayWords) {
			if (!isSpanText) {
				if (word.startsWith("&")) {
					type = AND;
					word = word.substring(1);
				} else if (word.startsWith("^")) {
					type = NOT;
					word = word.substring(1);
				} else {
					type = OR;
				}
				if (word.equals("(")) {
					isSpanText = true;
					continue;
				}

				addToList(type, word);
			}
			if (isSpanText) {
				if (word.equals(")")) {
					isSpanText = false;
					blockText = blockText.substring(0, blockText.length() - 1);
					
					addToList(type, blockText);
					blockText = "";
					continue;
				}
				blockText += word + " ";

			}

		}
		if (listOr.isEmpty()) listOr.add("");

	}

	private void addToList(TypeWord type, String word) {
		if (type.equals(OR)) {
			listOr.add(word);
		} else if (type.equals(AND)) {
			listAnd.add(word);
		} else if (type.equals(NOT)) {
			listNot.add(word);
		}
	}

	public boolean isPass(StringBuilder lineLog) {
		return isPass(lineLog.toString());
	}
	
	
	public boolean isPass(String lineLog) {
		return Stream.of(lineLog).parallel()
		.filter(line -> listOr.stream().anyMatch(or -> line.indexOf(or) >= 0))
		.filter(line -> listAnd.stream().allMatch(and -> line.indexOf(and) >= 0))
		.filter(line -> listNot.stream().noneMatch(not -> line.indexOf(not) >= 0))
		.findAny().isPresent();
		
	}	

	public boolean isPassIgnoreCase(String lineLog) {
		return Stream.of(lineLog).parallel()
		.filter(line -> listOr.stream().anyMatch(or -> containsIgnoreCase(line, or)))
		.filter(line -> listAnd.stream().allMatch(and -> containsIgnoreCase(line, and)))
		.filter(line -> listNot.stream().noneMatch(not -> containsIgnoreCase(line, not)))
		.findAny().isPresent();
		
	}	
}
