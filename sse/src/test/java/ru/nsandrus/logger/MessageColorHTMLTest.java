package ru.nsandrus.logger;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import ru.nsandrus.color.MessageColorHTML;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MessageColorHTML.class)
@WebAppConfiguration
public class MessageColorHTMLTest {

	String gf1 = "[#|2017-02-02T13:00:08.270+0300|SEVERE|sun-appserver2.1|sun-jms-binding.com.sun.jbi.jmsbc.OutboundMessageProcessor|_ThreadID=1407;_ThreadN"
			+ "javalang.Exception: BPCOR-6135: A fault was not handled in the process scope; Fault Name is {urn:at-consulting:integration:vimpelcom:managmen"
			+ "\tat com.sun.jbi.engine.bpel.BPELSEInOutThread.run(BPELSEInOutThread.java:187)\r\n" + "|#]";

	@Test
	public void notGF() {

		MessageColorHTML messageColorized = new MessageColorHTML();
		String colortext = messageColorized.getMessageColorized("Surkov\tAndrey\n\tDone");
		System.out.println("color text=" + colortext);
		assertEquals("<div class=\"gf\"><span class=\"column0\">Surkov&emsp;Andrey<br/>&emsp;Done</span></div>", colortext);
	}

	@Test
	public void notGFwhitespaces() {

		MessageColorHTML messageColorized = new MessageColorHTML();
		String colortext = messageColorized.getMessageColorized("Surkov\tAndrey\n     Done");
		System.out.println("color text=" + colortext);
		assertEquals("<div class=\"gf\"><span class=\"column0\">Surkov&emsp;Andrey<br/>&nbsp;&nbsp;&nbsp;&nbsp; Done</span></div>", colortext);
	}

	@Test
	public void notGFwhitespaces2() {

		MessageColorHTML messageColorized = new MessageColorHTML();
		String colortext = messageColorized.getMessageColorized("Surkov\tAndrey\n    Done");
		System.out.println("color text=" + colortext);
		assertEquals("<div class=\"gf\"><span class=\"column0\">Surkov&emsp;Andrey<br/>&nbsp;&nbsp;&nbsp;&nbsp;Done</span></div>", colortext);
	}

	@Test
	public void gf1() {

		MessageColorHTML messageColorized = new MessageColorHTML();
		String colortext = messageColorized.getMessageColorized(gf1);
		System.out.println("color text=" + colortext);
		assertEquals(
				"<p class=\"gf\"><span class=\"column0\">[#</span>|<span class=\"column1\">2017-02-02T13:00:08.270+0300</span>|<span class=\"SEVERE\">SEVERE</span>|<span class=\"column3\">sun-appserver2.1</span>|<span class=\"column4\">sun-jms-binding.com.sun.jbi.jmsbc.OutboundMessageProcessor</span>|<span class=\"column5\">_ThreadID=1407;_ThreadNjavalang.Exception: BPCOR-6135: A fault was not handled in the process scope; Fault Name is {urn:at-consulting:integration:vimpelcom:managmen&emsp;at com.sun.jbi.engine.bpel.BPELSEInOutThread.run(BPELSEInOutThread.java:187)<br/></span>|<span class=\"column6\">#]</span></p>",
				colortext);
	}
}
