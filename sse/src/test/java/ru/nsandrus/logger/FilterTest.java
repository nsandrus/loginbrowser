package ru.nsandrus.logger;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import ru.nsandrus.color.MessageColorHTML;
import ru.nsandrus.filter.FilterMessage;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MessageColorHTML.class)
@WebAppConfiguration
public class FilterTest {

	StringBuilder lineLog = new StringBuilder("[#|2017-02-02T13:00:08.270+0300|SEVERE|sun-appserver2.1|sun-jms-binding.com.sun.jbi.jmsbc.OutboundMessageProcessor|_ThreadID=1407;_ThreadN"
			+ "javalang.Exception: BPCOR-6135: A fault was not handled in the process scope; Fault Name is {urn:at-consulting:integration:vimpelcom:managmen"
			+ "\tat com.sun.jbi.engine.bpel.BPELSEInOutThread.run(BPELSEInOutThread.java:187)\r\n" + "|#]");

	StringBuilder lineLog2 = new StringBuilder("[#|2017-04-04T08:55:54.968+0300|FINE|sun-appserver2.1|com.sun.jbi.httpsoapbc.Soap11Normalizer|_ThreadID=798;"
			+ "_ThreadName=HTTPBC-OutboundAsyncReceiver-0;ClassName=com.sun.jbi.httpsoapbc.Soap11Normalizer;MethodName=processSoapBody;_RequestID=aa74e56a-e155-43a2-8dfb-383e27c1d9a4;"
			+ "|TYPE_DOC: false TYPE_RPC: false ELEM_DOC: true ELEM_RPC: false|#]");

	
	FilterMessage filter = new FilterMessage();
	
	@Test
	public void test1() {
		filter.setNewFilter("00:08");
		assertTrue(filter.isPass(lineLog));
	}

	@Test
	public void test2() {
		filter.setNewFilter("00:08 @.run");
		assertTrue(filter.isPass(lineLog));
	}
	
	@Test
	public void test3() {
		filter.setNewFilter("00:08 @.run ^MAMA");
		assertTrue(filter.isPass(lineLog));
	}
	
	@Test
	public void test4() {
		filter.setNewFilter("00:08 &.run ^MAMA &187");
		assertTrue(filter.isPass(lineLog));
	}

	@Test
	public void test5() {
		filter.setNewFilter("FAULT");
		assertFalse(filter.isPass(lineLog));
	}

	@Test
	public void test6() {
		filter.setNewFilter("00:08 &MAMA &187");
		assertFalse(filter.isPass(lineLog));
	}

	@Test
	public void test7() {
		filter.setNewFilter("( A fault was not handled in )");
		assertTrue(filter.isPass(lineLog));
	}

	@Test
	public void test8() {
		filter.setNewFilter("^( A fault was not handled in )");
		assertFalse(filter.isPass(lineLog));
	}

	@Test
	public void test9() {
		filter.setNewFilter("^WARNING");
		assertTrue(filter.isPass(lineLog));
	}

	@Test
	public void test10() {
		filter.setNewFilter("^ScoringAndVerification");
		assertTrue(filter.isPass(lineLog2));
	}

}
